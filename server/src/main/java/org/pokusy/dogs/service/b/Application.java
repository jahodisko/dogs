package org.pokusy.dogs.service.b;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class Application {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String args[]){
        try {
            logger.info("starting application B");
            ApplicationContext context = new FileSystemXmlApplicationContext("applicationContext.xml");
            RunnerB appRunner = (RunnerB) context.getBean("appRunner");
            appRunner.bark();
        } finally {
            logger.info("bye bye application B");
        }
    }
}
