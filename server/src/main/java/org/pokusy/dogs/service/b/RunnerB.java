package org.pokusy.dogs.service.b;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RunnerB {

    private static final Logger logger = LogManager.getLogger();
    private String propertyA;

    public RunnerB(String propertyB){
        this.propertyA = propertyB;
        logger.info("propertyB: {}", propertyB);
    }

    public void bark(){
            try {
                while (true) {
                    logger.info("vrrrrrrr haf baf");

                    Thread.sleep(2000L);
                }
            } catch (InterruptedException e) {
                logger.error("error while doing barking: {}", e.getMessage(), e);
            }

    }

}
