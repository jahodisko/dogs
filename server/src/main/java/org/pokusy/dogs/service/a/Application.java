package org.pokusy.dogs.service.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class Application {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String args[]){
        try {
            logger.info("starting application A");
            ApplicationContext context = new FileSystemXmlApplicationContext("applicationContext.xml");
            RunnerA appRunner = (RunnerA) context.getBean("appRunner");
            appRunner.startDoingSmth();
        } finally {
            logger.info("bye bye application A");
        }
    }
}
