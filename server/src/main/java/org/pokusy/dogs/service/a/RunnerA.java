package org.pokusy.dogs.service.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RunnerA {

    private static final Logger logger = LogManager.getLogger();
    private String propertyA;

    public RunnerA(String propertyA){
        this.propertyA = propertyA;
        logger.info("propertyA: {}", propertyA);
    }

    public void startDoingSmth(){
            try {
                while (true) {
                    logger.info("doing something usefull");

                    Thread.sleep(2000L);
                }
            } catch (InterruptedException e) {
                logger.error("error while doing something useful: {}", e.getMessage(), e);
            }

    }

}
