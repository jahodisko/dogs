# DOGS #

### compile ###
mvn package
* create and jar artifact in target

### prepare mvn dependencies ###
mvn dependency:copy-dependencies

### docker build ###
build parent dockerfile: sh build_dogs_docker.sh

build service a: sh build_a_docker.sh

build service b: sh build_b_docker.sh

### run docker image ###
run service a: (docker rm -f dogsa || true) && docker run --name dogsa -e "pokus=XXX" a:latest

run service b: (docker rm -f dogsb || true) && docker run --name dogsb -e "pokus=XXX" b:latest
